section .text
global _start


	
; caller-saved: rax, rcx, rdx, rsi, rdi , r8-r11
; callee saved: rbp, rsp, r12-r15, rbx

;rax - length
;rdi - first symbol
string_length:
	xor rax, rax
	.loop:
		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		ret

; rax - length
print_string:
	call string_length 
	mov rdx, rax 
	mov rsi, rdi 
	mov rax, 1 ; write and stdout
	mov rdi, 1 
	syscall 
	ret


print_char:
	push rdi ;save rdi
	mov rsi,rsp ;top element adress
	mov rdx,1 ; char - 1 byte
	mov rax,1
	mov rdi,1
	syscall
	pop rdi
   	ret


print_newline:
	mov rdi, 0xA
	jmp print_char
	

print_uint:
	xor rax, rax
	times 11 push word 0 ; 20 bytes + "0"
	mov rax,rdi ; rax - number
	mov r8,10 
	mov r9, rsp ;saving tops adress
	add rsp, 21
	.loop:
		xor rdx, rdx ; remainder
		dec rsp ; move link
		div r8 ; rdx - res 
		add rdx, "0" ; convert to ascii
		mov byte [rsp],dl ; move symbol to the stack
		test rax,rax 
		jnz .loop 
	mov rdi, rsp
	call print_string
	mov rsp, r9
	add rsp, 22 ;back to the previous link
	ret


print_int:
    	xor rax, rax
	test rdi, rdi
	jns print_uint ; if number > 0 then print uint
	push rdi
	mov rdi, "-"
	call print_char
	pop rdi
	neg rdi ; convert to positive number
	jmp print_uint
	




string_equals:
	xor rax, rax
	.loop:
		mov r8b, byte [rdi+rax] ; if we use r8 - mismatch in operand sizes
		mov r9b, byte[rsi+rax]
		cmp r8b, r9b
		jne .false
		test r8b, r8b
		jz .true
		inc rax 
		jmp .loop
	.true:
		xor rax, rax
		mov rax, 1
		ret
	.false: 
		xor rax, rax
		mov rax, 0
		ret


read_char:
	xor rax,rax
	push rax
	xor rdi,rdi
	mov rdi, 0
	mov rsi,rsp ; char's link
	mov rdx, 1 ; char - 1 byte
	syscall
   	mov rax,[rsp] 
	add rsp,8 ;
        ret
	

	
section .data
buffer: times 256 db 0

section .text


read_word:
	xor rdx, rdx
	xor rax, rax
	
	.loop:
		push rdi 
		push rsi
		push rdx ; save 
		call read_char
		pop rdx
		pop rsi
		pop rdi
		cmp rax, 0
		je .return
		cmp rdx, 0
		jne .read
		cmp rax, 0x9
		je .loop 
		cmp rax, 0xA
		je .loop
		cmp rax, 0x20
		je .loop	
	.read:
		cmp rdx, rsi
		je .error
		cmp rax, 0x9
		je .return
		cmp rax, 0xA
		je .return
		cmp rax, 0x20
		je .return
		mov [rdi+rdx], rax
		inc rdx
		jmp .loop
	.error:
		xor rax, rax
		ret
	.return:
		xor rax, rax
		mov byte[rdi+rdx], 0
		mov rax, rdi
		ret
	


parse_uint:
	xor rax, rax
	xor rdx, rdx
	xor rcx, rcx ;counter
	xor r9, r9
	mov r9, 10 ;basement
	xor r10, r10
	mov r10, "9"; max number
	xor r11, r11 ; buffer

	
	.loop:
		mov r11b, byte[rdi, rcx]
		cmp r11b, "0"
		js .break
		cmp r10b, r11b
		js .break
		sub r11, "0"
		mul r9
		add rax, r11
		inc rcx
		test rax, rax
		jnz .loop
	.break:
		mov rdx, rcx
		ret


parse_int:
	xor rax, rax
	push r12
	xor r12, r12
	cmp rdi, 0
	je .end
	cmp byte[rdi], "-"
	jnz .inverse
	mov r12, -1
	inc rdi
	.inverse:
		call parse_uint
		xor rax, r12 :
		sub rax, r12
		sub rdx, r12
		pop r12
		ret
	.end:
		ret

string_copy:
	xor rax, rax
	call string_length
	inc rax
	cmp rax, rdx
	ja .error
	xor rcx , rcx
	.copy:
		mov rdx, [rdi + rcx]
		mov [rsi + rcx], rdx
		inc rcx
		cmp rax, rcx
		jne .copy
		ret
	.error:
		xor rax, rax
		ret
